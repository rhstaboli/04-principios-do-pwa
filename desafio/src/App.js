import React, { Component } from 'react';
import './App.css';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
 

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit = event => {
    const meta = 2;
    const quant = this.state.value
		event.preventDefault()
		if (quant < meta) {
			alert('Você Necessita tomar mais '+ (2 - quant) + ' Litros de Agua')
		} else if (quant == meta) {
			alert('Parabens você esta hidratado')
		} else if (quant > meta ) {
			alert('Cuidado, você esta SuperHidratado, amanha você tomar menos ' + (quant-2) + ' Litros de Agua')
		} else {
			alert('Informação Invalida')
		}		
	}

  render() {
    return (
      <div>
      <header className="Header">Hidrate-Se</header>
      <body className="body"> Vocé deve tomar 2 litros de agua por dia 
      <form onSubmit={this.handleSubmit}>
            <label>
             Quantos litros de agua vc tomou hoje? 
             <input type="text" value={this.state.value} onChange={this.handleChange} />
            </label>
            <input type="submit" value="Submit"/>
      </form>
      </body>
      </div>
    );
  }
}

export default App;
